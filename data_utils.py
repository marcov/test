import torch
import torchtext.vocab as vocab
import spacy
import re
import numpy as np
import os
import collections
 
from spacy.attrs import ORTH, LEMMA
from spacy.tokenizer import Tokenizer
from collections import Counter

nlp_en = spacy.load('en_core_web_sm')
glove = vocab.GloVe(name='6B', dim=100)

data_path = "./ptb" # args
data_prefix = "ptb"
data_sample_prefix = "ptb.simple"

sample_path = os.path.join(data_path, data_prefix + ".sample.txt") 
train_path = os.path.join(data_path, data_prefix + ".train.txt")
valid_path = os.path.join(data_path, data_prefix + ".valid.txt")
text_path = os.path.join(data_path, data_prefix + ".test.txt")
temp_path = os.path.join(data_path, data_prefix + ".temp.txt")
vocab_path = os.path.join(data_path, data_prefix + ".vocab.txt")

gene_data = os.path.join(data_path, "gene.data")
real_data = os.path.join(data_path, "rela.data")
eval_data = os.path.join(data_path, "eval.data")

contractions_dict = {
    "ain 't": "am not",
    "aren 't": "are not",
    "can 't": "cannot",
    "can 't 've": "cannot have",
    "'cause": "because",
    "could 've": "could have",
    "couldn 't": "could not",
    "couldn 't 've": "could not have",
    "didn 't": "did not",
    "doesn 't": "does not",
    "don 't": "do not",
    "hadn 't": "had not",
    "hadn't've": "had not have",
    "hasn 't": "has not",
    "haven 't": "have not",
    "he 'd": "he had / he would",
    "he'd've": "he would have",
    "he'll": "he shall / he will",
    "he'll've": "he shall have / he will have",
    "he's": "he has / he is",
    "how 'd": "how did",
    "how'd'y": "how do you",
    "how'll": "how will",
    "how's": "how has / how is",
    "i'd": "I had / I would",
    "i'd've": "I would have",
    "i'll": "I shall / I will",
    "i'll've": "I shall have / I will have",
    "i 'm": "I am",
    "i 've": "I have",
    "isn 't": "is not",
    "it'd": "it had / it would",
    "it'd've": "it would have",
    "it'll": "it shall / it will",
    "it'll've": "it shall have / it will have",
    "it's": "it has / it is",
    "let's": "let us",
    "ma'am": "madam",
    "mayn't": "may not",
    "might've": "might have",
    "mightn't": "might not",
    "mightn't've": "might not have",
    "must've": "must have",
    "mustn't": "must not",
    "mustn't've": "must not have",
    "needn't": "need not",
    "needn't've": "need not have",
    "o'clock": "of the clock",
    "oughtn't": "ought not",
    "oughtn't've": "ought not have",
    "shan't": "shall not",
    "sha'n't": "shall not",
    "shan't've": "shall not have",
    "she'd": "she had / she would",
    "she'd've": "she would have",
    "she'll": "she shall / she will",
    "she'll've": "she shall have / she will have",
    "she 's": "she has / she is",
    "should 've": "should have",
    "shouldn 't": "should not",
    "shouldn't've": "should not have",
    "so 've": "so have",
    "so 's": "so as / so is",
    "that'd": "that would / that had",
    "that'd've": "that would have",
    "that's": "that has / that is",
    "there'd": "there had / there would",
    "there'd've": "there would have",
    "there's": "there has / there is",
    "they'd": "they had / they would",
    "they'd've": "they would have",
    "they'll": "they shall / they will",
    "they'll've": "they shall have / they will have",
    "they 're": "they are",
    "they 've": "they have",
    "to've": "to have",
    "wasn 't": "was not",
    "we 'd": "we had / we would",
    "we'd've": "we would have",
    "we 'll": "we will",
    "we'll've": "we will have",
    "we 're": "we are",
    "we 've": "we have",
    "weren 't": "were not",
    "what'll": "what shall / what will",
    "what'll've": "what shall have / what will have",
    "what're": "what are",
    "what's": "what has / what is",
    "what've": "what have",
    "when's": "when has / when is",
    "when've": "when have",
    "where'd": "where did",
    "where's": "where has / where is",
    "where've": "where have",
    "who'll": "who shall / who will",
    "who'll've": "who shall have / who will have",
    "who's": "who has / who is",
    "who've": "who have",
    "why's": "why has / why is",
    "why've": "why have",
    "will've": "will have",
    "won 't": "will not",
    "won 't 've": "will not have",
    "would 've": "would have",
    "wouldn 't": "would not",
    "wouldn 't 've": "would not have",
    "y 'all": "you all",
    "y 'all 'd": "you all would",
    "y 'all 'd 've": "you all would have",
    "y 'all 're": "you all are",
    "y 'all 've": "you all have",
    "you'd": "you had / you would",
    "you'd've": "you would have",
    "you'll": "you shall / you will",
    "you'll've": "you shall have / you will have",
    "you 're": "you are",
    "you 've": "you have"
}

contractions_re = re.compile('(%s)' % '|'.join(contractions_dict.keys()))

def _expand_contractions(s, contractions_dict=contractions_dict):
    def replace(match):
        return contractions_dict[match.group(0)]
    return contractions_re.sub(replace, s)

def _preprocess_file(filename):
    with open(filename, "r+") as input:
        with open(temp_path, "w") as temp:
            for line in input.readlines():
                line = _expand_contractions(line)  
                line = line.lower()
                for word in line.split():                                                                                         
                    try:     
                        glove.vectors[glove.stoi[word]]                        
                        temp.write(word + " ")
                    except KeyError:                        
                        temp.write(" <unk> ")
                temp.write("\n")

def _tokenizer():
    tokenizer = Tokenizer(nlp_en.vocab)
    # eos_case = [{ORTH: u'<eos>', LEMMA: u'<eos>'}]
    unk_case = [{ORTH: u'<unk>', LEMMA: u'<unk>'}]    
    # tokenizer.add_special_case(u'<eos>', eos_case)    
    tokenizer.add_special_case(u'<unk>', unk_case)       
    return tokenizer

def _read_words(filename):    
    tokens = []
    tokenizer = _tokenizer()
    with open(filename, 'r') as f:
        for line in f.readlines():  # regex remove space before word
            # line = line.replace('\n', ' <eos>')                              
            doc = tokenizer(line)
            for token in doc:
                tokens.append(token.text)
    return tokens

def _build_vocab(vocab_file, filename, logdir=None):

    try:
        vocab_file = open(vocab_path, 'r')

        word_to_id = {}
        lines = vocab_file.read().splitlines()
        for line in lines:
            entry = line.split()
            word_to_id.update({entry[0]: int(entry[1])})

    except IOError:
        vocab_file = open(vocab_path, 'w')

        # metadata_path = os.path.join(logdir, "metadata.tsv")
        # projector_path = os.path.join(logdir, "projector_config.pbtxt")
        # metadata_file = open(metadata_path, 'w')
        # projector_file = open(projector_path, 'w')                

        data = _read_words(filename)        
        counter = collections.Counter(data)
        count_pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))
        words, _ = list(zip(*count_pairs))
        word_to_id = dict(zip(words, range(len(words))))

        # projector_file.write("embeddings {tensor_name: 'Model/embedding' metadata_path:'metadata.tsv'}")

        for word, id in word_to_id.items():
            line = "%s %s" % (word, id)
            vocab_file.write(line + "\n")
            # metadata_file.write(word + "\n")

    return word_to_id

# def _create_dataset(filename):
    

def _glove_embeddings(word_to_id):    
    embeddings = []
    for word, id in word_to_id.items():        
        try:     
            vector = glove.vectors[glove.stoi[word]]
            embeddings.append(vector)
        except KeyError:
            print(word)
            embeddings.append(np.zeros(100))
    return embeddings


_preprocess_file(sample_path)
word_to_id = _build_vocab(vocab_path, temp_path)
print("vocab_size: ", len(word_to_id))
embeddings = _glove_embeddings(word_to_id)

