###############################################################################
# Language Modeling on Penn Tree Bank
#
# This file generates new sentences sampled from the language model
#
###############################################################################

import argparse

import torch
import torch.nn as nn
from torch.autograd import Variable

import target.data

class TargetModel():

    def __init__(self, seed, checkpoint, data, hidden_dim, cuda=False):   
                 
        self.cuda = cuda        
        
        # Set the random seed manually for reproducibility.  
        if self.cuda:
            torch.cuda.manual_seed(seed)
            torch.set_default_tensor_type('torch.cuda.FloatTensor')            
        else:
            torch.manual_seed(seed)        

        with open(checkpoint, 'rb') as f:    
            if self.cuda:        
                self.model, _, _ = torch.load(f, map_location='cuda:0')  
            else:
                self.model, _, _ = torch.load(f, map_location=lambda storage, loc: storage)  
            
        self.model.eval()
        
        with open(data, 'rb') as fn:
            if self.cuda:
                self.model.cuda()
                self.corpus = torch.load(fn)
            else:
                self.model.cpu() 
                self.corpus = torch.load(fn, map_location=torch.device('cpu'))

        self.ntokens = len(self.corpus.dictionary)
        self.hidden_dim = hidden_dim
        self.lin = nn.Linear(hidden_dim, self.ntokens)
        self.softmax = nn.LogSoftmax()

    def _init_hidden(self):
        self.hidden = self.model.init_hidden(1)

    def forward(self, input):                
        # for b in input.size(0):
        #     for i in input.size(1):
        batch_size = input.size(0)
        seq_len = input.size(1)        
        hidden = self.model.init_hidden(seq_len)          
        output, hidden = self.model(input, hidden)        
        pred = self.softmax(self.lin(output.contiguous().view(-1, self.hidden_dim)))    
        return pred

    def sample(self, batch_size, seq_len):
        res = []        
        for b in range(batch_size):
            input = Variable(torch.zeros((1, 1)).long(), requires_grad=False)            
            if self.cuda:
                input = input.cuda()
            hidden = self.model.init_hidden(1)
            samples = []
            for i in range(seq_len):
                output, hidden = self.model(input, hidden)
                word_weights = output.squeeze().data.div(1.0).exp().cpu()
                word_idx = torch.multinomial(word_weights, 1)[0]                
                input.data.fill_(word_idx)                              
                samples.append(word_idx)
            s = torch.stack(samples)                                                        
            res.append(s)              
        r = torch.stack(res)                                      
        return r

